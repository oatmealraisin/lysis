"""
Functions relating to the bot.
"""

from datetime import date, datetime
from dateutil import parser
import logging
import re
import typing

import discord

from .predict import get_predictions, next_color_glow, next_color_dim
from .util import (aberoth_time, format_predictions, preprocess_input,
                   format_predictions_vague, aberoth_to_string)

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

trivia: dict[str, str] = {
    'hello': 'Wecome, {}. How can I help?',
    'hi': 'Wecome, {}. How can I help?',
    'yo': 'Wecome, {}. How can I help?',
    'wassup': 'Wecome, {}. How can I help?',
    'wazzup': 'Wecome, {}. How can I help?',
}

sub_re_when_is = '(when(\'s| is| are)? )'
sub_re_color = '(white|black|green|red|purple|yellow|cyan|blue)'
sub_re_whats_happening = '(what(\'s| is)? )?(((glow|happen)ing|dim(med)?) )'
sub_re_what_happened = '(what )?(((glow|happen)ed|dim(med)?) )'
sub_re_date = '(\d\d\d\d-\d\d-\d\d|\d\d/\d\d/\d\d\d\d)'

re_color = re.compile(sub_re_color)
re_date = re.compile(sub_re_date)

re_get_today = re.compile("|".join([
    '{}?today'.format(sub_re_whats_happening),
]))

re_get_yesterday = re.compile("|".join([
    '{}?yesterday'.format(sub_re_whats_happening),
]))

re_get_tomorrow = re.compile("|".join([
    '{}?tomorrow'.format(sub_re_whats_happening),
]))

re_get_this_week = re.compile("|".join([
    '{}?(this )?week'.format(sub_re_whats_happening),
]))

re_get_next_week = re.compile("|".join([
    '{}?next week'.format(sub_re_whats_happening),
]))

re_get_date = re.compile("|".join([
    '{}?{}'.format(sub_re_whats_happening, sub_re_date),
]))

re_next_color_glow = re.compile("|".join([
    '{}?(the )?next (?P<count>\d )?(?P<color>{})( glow(s)?)?'.format(sub_re_when_is, sub_re_color),
]))

re_next_color_dim = re.compile("|".join([
    '{}?(the )?next (?P<count>\d )?(?P<color>{}) dim(s)?'.format(sub_re_when_is, sub_re_color),
]))

#re_next_multi_glow = re.compile("|".join([
#    '{}{} (and )?{}( aligned| glow(ing)?)?( next)'.format(sub_re_when_is, sub_re_color, sub_re_color),
#]))

BETA = 0
HIGH = 1
NORMAL = 2
LOW = 3
NO_PERM = 1000

def _authorized(level: int, message: discord.Message) -> bool:
    logger.debug("Checking auth level for {}".format(message.guild.id))
    auth_level: dict[int,int] = {
        870383494327570512: BETA, # GDC
        825412898372321290: NORMAL, # Banabread
        878722012351180801: BETA, # Red/Blue
        689863485554819129: LOW, # Legion PVP
        370780258141601792: NO_PERM, # Glowbot
    }

    if message.guild.id not in auth_level:
        # We don't know about this guild
        return False

    return auth_level[message.guild.id] <= level


def parse_command(message: discord.Message) -> typing.Callable[[typing.Any], str]:
    content: list[str] = preprocess_input(message.content)
    if len(content) ==  1:
        return greeting
    if len(content) == 2 and content[1].lower() in trivia:
        return get_trivia
    if re_get_today.match(" ".join(content[1:]).lower()) is not None:
        return get_today
    if re_get_yesterday.match(" ".join(content[1:]).lower()) is not None:
        return get_yesterday
    if re_get_tomorrow.match(" ".join(content[1:]).lower()) is not None:
        return get_tomorrow
    if re_get_date.match(" ".join(content[1:]).lower()) is not None:
        return get_date
    if re_next_color_dim.match(" ".join(content[1:]).lower()) is not None:
        return get_next_color_dim
    if re_next_color_glow.match(" ".join(content[1:]).lower()) is not None:
        return get_next_color

    return no_match


def not_authorized(message: discord.Message) -> str:
    """
    Give a message if the user tries to access a command from a Guild without
    sufficient permissions.
    """
    not_authorized_message: str = "Greetings, {}. I can't tell you about that."
    return not_authorized_message.format(message.author.mention)


async def no_match(message: discord.Message) -> str:
    """
    Provided when we don't know what they're talking about.
    """
    no_match_message: str = "Welcome, {}. That does not interest me."
    return no_match_message.format(message.author.mention)


async def greeting(message: discord.Message) -> str:
    """
    They only said our name.
    """
    return trivia['hi'].format(message.author.mention)


async def get_trivia(message: discord.Message) -> str:
    """
    Return some trivia that you could get from talking to the NPC in game.
    Generally only one-word phrases
    """
    content: list[str] = preprocess_input(message.content)
    return trivia[content[1].lower()].format(message.author.mention)


async def get_today(message: discord.Message) -> str:
    """
    Return a summary of orb events happening today.

    Today is defined as the unix midnight of today to the unix midnight of
    tomorrow.
    """
    if not _authorized(HIGH, message):
        return not_authorized(message)

    today = date.today()
    today_dt = datetime(year=today.year, month=today.month, day=today.day)
    start = aberoth_time(int(today_dt.timestamp()))
    end = start + 24 * 60 * 60 * 10

    predictions = get_predictions(start, end)

    if len(predictions) == 0:
        return "There is nothing glowing today."

    logger.debug(len(predictions))

    pred_formatted: str = ""

    if not _authorized(BETA, message):
        pred_formatted = format_predictions_vague(predictions)
    else:
        pred_formatted = format_predictions(predictions)

    msg: str = "I'm glad you asked, {}. Today will have these alignments:\n{}".format(
        message.author.mention, pred_formatted
    )

    return msg


async def get_yesterday(message: discord.Message) -> str:
    if not _authorized(BETA, message):
        return not_authorized(message)

    today = date.today()
    today_dt = datetime(year=today.year, month=today.month, day=today.day)
    end = aberoth_time(int(today_dt.timestamp()))
    start = end - 24 * 60 * 60 * 10

    predictions = get_predictions(start, end)

    if len(predictions) == 0:
        return "There is nothing glowing today."

    logger.debug(len(predictions))

    if not _authorized(BETA, message):
        return format_predictions_vague(predictions)

    return format_predictions(predictions)



async def get_tomorrow(message: discord.Message) -> str:
    """
    Return a summary of orb events happening tomorrow.

    Tomorrow is defined as the unix midnight of tomorrow to the unix midnight of
    the day after.
    """
    if not _authorized(HIGH, message):
        return not_authorized(message)

    today = date.today()
    today_dt = datetime(year=today.year, month=today.month, day=today.day)
    start = aberoth_time(int(today_dt.timestamp())) + 24 * 60 * 60 * 10
    end = start + 24 * 60 * 60 * 10

    predictions = get_predictions(start, end)

    if len(predictions) == 0:
        return "There is nothing glowing today."

    logger.debug(len(predictions))

    if not _authorized(BETA, message):
        return format_predictions_vague(predictions)

    return format_predictions(predictions)


async def get_date(message: discord.Message) -> str:
    if not _authorized(HIGH, message):
        return not_authorized(message)

    date_str: str = re_date.search(message.content)[1]

    start_dt = parser.parse(date_str)
    logger.info(start_dt)
    start = aberoth_time(int(start_dt.timestamp()))
    end = start + 24 * 60 * 60 * 10
    logger.info(aberoth_to_string(start))
    logger.info(aberoth_to_string(end))

    predictions = get_predictions(start, end)

    if len(predictions) == 0:
        return "There is nothing glowing today."

    logger.debug(len(predictions))

    if not _authorized(BETA, message):
        return format_predictions_vague(predictions)

    return format_predictions(predictions)


async def get_next_color(message: discord.Message) -> str:
    if not _authorized(HIGH, message):
        return not_authorized(message)

    today = date.today()
    today_dt = datetime(year=today.year, month=today.month, day=today.day)
    start = aberoth_time(int(today_dt.timestamp()))
    logger.debug(aberoth_to_string(start))

    color = re_next_color_glow.search(message.content).group("color")
    logger.debug(color)

    count_search = re_next_color_glow.search(message.content).group("count")
    if len(count_search) > 1:
        count = count_search[0]

    try:
        predictions = next_color_glow(color, start, count)
    except NotImplementedError:
        return "I'm sorry, I cannot do that yet."

    if len(predictions) == 0:
        return "I don't believe there will be another glow for {}, {}. This is most concerning.".format(color, message.author.mention)

    logger.debug(len(predictions))

    if not _authorized(BETA, message):
        return format_predictions_vague(predictions)

    return format_predictions(predictions)


async def get_next_color_dim(message: discord.Message) -> str:
    if not _authorized(HIGH, message):
        return not_authorized(message)

    today = date.today()
    today_dt = datetime(year=today.year, month=today.month, day=today.day)
    start = aberoth_time(int(today_dt.timestamp()))
    logger.debug(aberoth_to_string(start))

    color = re_color.search(message.content).group("color")
    logger.debug(color)

    try:
        predictions = next_color_dim(color, start)
    except NotImplementedError:
        return "I'm sorry, I cannot do that yet!"

    if len(predictions) == 0:
        return "I don't believe there will be another dim for {}, {}. This is most concerning.".format(color, message.author.mention)

    logger.debug(len(predictions))

    if not _authorized(BETA, message):
        return format_predictions_vague(predictions)

    return format_predictions(predictions)
