"""
Utility functions for the Lysis bot
"""

from collections import deque
from datetime import datetime
from itertools import chain
import logging
import re
import pytz


discord_id = re.compile(r'^(?P<o><)?(@!?)?(?P<id>\d{15,20})(?(o)>|)$')

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

BEGIN_TIME_TWOSHIELDS = 72144000
BEGIN_TIME_KILLBEES = 72147600
BEGIN_TIME_KILLBEES_ADJUSTED = BEGIN_TIME_KILLBEES - 3600 * 3
BEGIN_TIME_WIKI = 72151200
BEGIN_TIME = BEGIN_TIME_WIKI


def aberoth_time(timestamp: int) -> int:
    """
    Return the Aberoth adjusted timestamp of a given Unix timestamp
    """
    return int(10 * ( timestamp - BEGIN_TIME ) )


def aberoth_to_string(timestamp: int) -> str:
    """
    Returns a string repr of the given Aberoth timestamp, in UTC.
    """
    return real_to_string(real_time(timestamp))


def aberoth_to_string_tz(timestamp: int, tz: str) -> str:
    """
    Returns a string repr of the given Aberoth timestamp, in the given timezone.
    """
    return real_to_string_tz(real_time(timestamp), tz)


def real_time(timestamp: int) -> int:
    """
    Returns a Unix timestamp for a given Aberoth time.
    """
    return int((timestamp / 10.0) + BEGIN_TIME)


def real_to_string(timestamp: int) -> str:
    """
    Returns a string repr of the given unix timestamp, in UTC
    """
    utc = pytz.utc
    utc_dt = utc.localize(datetime.utcfromtimestamp(timestamp))

    return utc_dt.strftime('%Y-%m-%d %H:%M')
    #return datetime.utcfromtimestamp(lysis.real_time(i)).strftime('%Y-%m-%d %H:%M:%S')


def real_to_string_tz(timestamp: int, tz: str) -> str:
    """
    Returns a string repr of the given unix timestamp in a specific timezone.
    """

    utc = pytz.utc
    zone = pytz.timezone(tz)
    utc_dt = utc.localize(datetime.utcfromtimestamp(timestamp))
    zone_dt = utc_dt.astimezone(zone)

    return zone_dt.strftime('%Y-%m-%d %H:%M')


def is_discord(ident):
    """
    Checks if the input is a valid Discord mention/id
    """
    return (
        (
            isinstance(ident, int)
            # TODO: Get a better test
            and ident >= 100000000000000
        ) or (
            isinstance(ident, str) and discord_id.match(ident) is not None
        )
    )


def get_discord_id(ident):
    """
    Returns the integer value Discord identity
    """
    logger.debug("Getting ID from {}".format(ident))

    if isinstance(ident, int) and 1000000000000000000 > ident >= 100000000000000000:
        logger.debug("Passed an int, returning it as is")
        return ident
    if not is_discord(ident):
        logger.debug("Not a discord ID: {}".format(ident))
        return 0
    logger.debug("Returning {}".format(discord_id.match(ident).groupdict()["id"]))

    return int(discord_id.match(ident).groupdict()["id"])


def preprocess_input(content: str) -> list[str]:
    """
    Runs a client input through a series of checks and changes.
    """
    output = []
    changed = False

    if isinstance(content, str):
        content = content.split()

    for i, word in enumerate(content):
        if isinstance(word, (float,int)):
            output.append(word)
            continue
        word = word.lower().replace('?', '')
        if is_discord(word):
            output.append(get_discord_id(word))
        elif word == "":
            continue
        else:
            output.append(word)

    return output


def format_predictions_vague(predictions) -> str:
    """
    A utility function that takes a series of predictions and formats them into
    one message, but with limited accuracy, ready to send to Discord.
    """

    msg: str = "\n"
    for prediction in predictions:
        logger.debug(" ".join(prediction[0]))
        state = "__dimmed__" if prediction[-1] else "__glowing__"
        start = real_to_string_tz(real_time(prediction[1]), "US/Eastern")
        end = real_to_string_tz(real_time(prediction[2]), "US/Eastern")

        if len(prediction[0]) == 1:
            msg += "> **" + list(prediction[0])[0].capitalize() + "**"
        elif len(prediction[0]) == 2:
            msg += "> **" + "** and **".join(list(prediction[0])).capitalize() + "**"
        else:
            msg += "> **" + "**, **".join(list(prediction[0])[:-1]).capitalize()
            msg += "**, and " + "**" + list(prediction[0])[-1] + "**"

        msg += " will be **{}**.\n\n".format(state)

    return msg


def format_predictions(predictions, vague=False) -> str:
    """
    A utility function that takes a series of predictions and formats them into
    one message, ready to send to Discord.
    """

    msg: str = "\n"
    for prediction in predictions:
        logger.debug(" ".join(prediction[0]))
        state = "__dimmed__" if prediction[-1] else "__glowing__"
        start = real_time(prediction[1])
        end = real_time(prediction[2])

        if len(prediction[0]) == 1:
            msg += "> **" + list(prediction[0])[0].capitalize() + "**"
        elif len(prediction[0]) == 2:
            msg += "> **" + "** and **".join(prediction[0]).capitalize() + "**"
        else:
            print(type(prediction))
            msg += "> **" + "**, **".join(list(prediction[0])[:-1]).capitalize()
            msg += " **, and " + "**" + list(prediction[0])[-1] + "**"

        if not vague:
            msg += " will be **{}** from <t:{}:f> until <t:{}:t>.\n".format(
                state, start, end)

    return msg


def total_size(obj):
    """ Returns the approximate memory footprint an object and all of its contents.

    Automatically finds the contents of the following builtin containers and
    their subclasses:  tuple, list, deque, dict, set and frozenset.

    https://code.activestate.com/recipes/577504/
    """
    dict_handler = lambda d: chain.from_iterable(d.items())
    all_handlers = {tuple: iter,
                    list: iter,
                    deque: iter,
                    dict: dict_handler,
                    set: iter,
                    frozenset: iter,
                   }
    seen = set()                      # track which object id's have already been seen
    default_size = getsizeof(0)       # estimate sizeof object without __sizeof__

    def sizeof(obj):
        if id(obj) in seen:       # do not double count the same object
            return 0
        seen.add(id(obj))
        size = getsizeof(obj, default_size)

        for typ, handler in all_handlers.items():
            if isinstance(obj, typ):
                size += sum(map(sizeof, handler(obj)))
                break
        return size

    return sizeof(obj)
