"""
Lysis bot is a bot for predictions
"""

import logging

from .visualization import polar
from .alignments import is_dim, has_glow, has_dim, get_glowing, get_alignments
from .predict import get_predictions
from .util import aberoth_time, real_time, real_to_string_tz, preprocess_input
from .util import format_predictions, get_discord_id
from .bot import get_today, get_tomorrow, parse_command


logger = logging.getLogger(__name__)
logging.basicConfig()
logger.setLevel(logging.DEBUG)
